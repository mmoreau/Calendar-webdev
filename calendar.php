<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Calendrier</title>
		<link rel="stylesheet" type="text/css" href="calendar.css">
	</head>
	<body>
		<?php 
			date_default_timezone_set("Europe/Paris");

			$months = [
				"Janvier", 
				"Février",
				"Mars",
				"Avril",
				"Mai", 
				"Juin",
				"Juillet",
				"Août",
				"Septembre",
				"Octobre",
				"Novembre",
				"Décembre"
			];


			function getDay($month, $year): int {

				$break = true;
				$i = 1;
				$count = 0;

				while ($break) {

					$newDate = mktime(0, 0, 0, $month, (28 + $i), $year);
					$newMonth = date("m", $newDate);

					if ($newMonth > $month) {
						$break = false;
					} 

					if ($newMonth == $month) {
						++$i;
						++$count;
					}
				}


				return (28 + $count);
			}


			function getFirstDayL($month, $year): int {
				return strftime("%u", strtoTime("$year-$month-1"));
			}


			$day = date("d");
			$month = date("m");
			$year = date("Y");

			$n = getFirstDayL($month, $year);
			$getDay = getDay($month, $year);
		?>
		
		<div id="calendar">

			<?php echo "<h1>".$months[($month - 1)]." $year</h1>"; ?>

			<table>
				<tr>
					<td>Lun</td>
					<td>Mar</td>
					<td>Mer</td>
					<td>Jeu</td>
					<td>Ven</td>
					<td>Sam</td>
					<td>Dim</td>
				</tr>

			<?php
				for ($i = 1; $i <= $getDay + 7; ++$i) {

					if ($i <= 7) {

						if ($i <= ($n - 1)) {
							echo "<td style='padding:1vh;'> </td>";
						}

					}

					if ($i > 7) {

						if (($i - 7) == intval($day)) {
							echo "<td style='padding:1vh;background-color:#2874a6;text-align:center;border-radius:2vh;color:white;'>".($i - 7)."</td>";
						} 

						if (($i - 7) != intval($day)) {
							echo "<td style='padding:1vh;text-align:center;'>".($i - 7)."</td>";
						}

						if (($i - 7) == $getDay) {
							break;
						}

						if (($i + ($n - 1)) % 7 == 0) {
							echo "<tr></tr>";
						}
					}
				}
			?>
			</table>
		</div>

	</body>	
</html>